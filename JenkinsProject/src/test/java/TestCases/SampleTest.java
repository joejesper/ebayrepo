package TestCases;

import org.testng.annotations.Test;

import base.Base;

public class SampleTest extends Base{
	@Test
	public void testCase1() {
		System.out.println("this is testCase 1");
	}

	@Test
	public void testCase2() {
		System.out.println("this is testCase 2");
	}
	
	@Test
	public void testCase3() {
		System.out.println("this is test case 3");
	}
}
