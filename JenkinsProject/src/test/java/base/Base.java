package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Base {
	
 WebDriver driver;

	@BeforeMethod
	public void setup() throws InterruptedException {
		
		/*
		 * System.setProperty("webdriver.chrome.driver", "C:\\java\\chromedriver.exe");
		 * driver = new ChromeDriver(); driver.get("https://google.com");
		 * driver.manage().window().maximize();
		 */
		 
		
		
		  //ChromeOptions option = new ChromeOptions();
		  //option.setHeadless(false);
		  WebDriverManager.chromedriver().setup(); 
		  driver = new ChromeDriver();
		  Thread.sleep(5000);
		  driver.get("https://www.google.com/");
		 
		
	}

	

	@AfterMethod
	public void tearDown() {
		//driver.close();
	}
}
