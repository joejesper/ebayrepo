package base;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Base {
	
	
	public static WebDriver driver;
	
	@BeforeSuite
	public void setup() {
		// WebDriver driver = new ChromeDriver(); //here we use ChromeDriver to
		// instantiate driver variable.

		ChromeOptions option = new ChromeOptions(); // to customize Browser
		option.setHeadless(false);
		WebDriverManager.chromedriver().setup();// to execute without any exe file or binary file
		driver = new ChromeDriver(option);
	}// end
	
	@AfterSuite
	public void tearDown() {
		driver.close();
	}
	
	
	public void get(String string) {
		driver.get(string);
	}
	
	public void click(By locator) {
		driver.findElement(locator).click();
	}
	
	
	public void sendkeys(By locator,String value) {
		driver.findElement(locator).sendKeys(value + Keys.ENTER);
		
	}
	
}
