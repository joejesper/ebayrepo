package testCases;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import base.Base;

public class Test_open_the_browser extends Base {
	
	@Test(priority=1)
	public void given_I_am_in_craigslist_org() {
		get("https://newyork.craigslist.org/");
		
	}

	@Test(priority=2)
	public void then_I_enter_camera_on_the_search_box() {
		sendkeys(By.id("query"),"camera");
		System.out.println("search for camera ");
	}

}
